<?php
require "fpdf/fpdf.php";
require "logica/Tienda_Producto.php";

$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Parcial", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte Productos", 0, 2, "C");
$pdf->Ln();


$idP = $_GET["idProducto"];
$Tienda_Poducto = new Tienda_Producto("","",$idP,"");
$producto = $Tienda_Poducto -> consultarProductos_Tienda();


$pdf -> SetFont("Courier", "B", 10);
$pdf->Cell(50,12,"Tienda",1,0,'L',0);
$pdf->Cell(20,12,"Producto",1,0,'L',0);
$pdf->Ln();
$i=1;
foreach ($producto as $filas) {
    $pdf->Cell(50,40,$filas -> getIdProducto(),1,0,'L',0);
    $pdf->Cell(20,40,$filas -> getCantidad(),1,0,'L',0);

$pdf->Ln();
}


$pdf -> Output();

?>
