<nav class="navbar navbar-expand-md navbar-light bg-info">

	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Producto</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Crear</a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoTodos.php") ?>">Consultar
          </a>
				</div></li>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
      		<ul class="navbar-nav mr-auto">
      			<li class="nav-item dropdown active"><a
      				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
      				role="button" data-toggle="dropdown" aria-haspopup="true"
      				aria-expanded="false">Tienda</a>
      				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
      					<a class="dropdown-item"
      						href="index.php?pid=<?php echo base64_encode("presentacion/tienda/crearTienda.php") ?>">Crear</a>
      					<a class="dropdown-item"
      						href="index.php?pid=<?php echo base64_encode("presentacion/tienda/consultarTiendaTodos.php") ?>">Consultar
                </a>
      				</div></li>
		</ul>
</nav>
