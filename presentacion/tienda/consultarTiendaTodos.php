<?php
$tienda = new Tienda();
$tiendas = $tienda -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Tiendas</h4>
				</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Direccion</th>
							<th>Añadir</th>
							<th>PDF</th>
						</tr>
						<?php
						$i=1;
						foreach($tiendas as $tiendaActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $tiendaActual -> getNombre() . "</td>";
						    echo "<td>" . $tiendaActual -> getDireccion() . "</td>";
								echo "<td><a href='index.php?pid=". base64_encode("presentacion/tienda/añadirT.php") . "&idTienda=" . $tiendaActual -> getIdTienda(). "' data-toggle='tooltip' data-placement='left' title='Añadir'><span class='fas fa-edit'></span></a></td>";
								echo "<td><a href='reporteTiendasPDF.php'" . "&idTienda=" . $tiendaActual -> getIdTienda(). "' data-toggle='tooltip' data-placement='left' title='PDF'><span class='fas fa-file-pdf'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
