<?php

$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}

$direccion = "";
if(isset($_POST["direccion"])){
    $direccion = $_POST["direccion"];
}

if(isset($_POST["crear"])){

    $tienda = new Tienda("", $nombre, $direccion);
    $tienda -> insertar();
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Insertar Tienda</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						 Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/tienda/crearTienda.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Direccion</label>
							<input type="text" name="direccion" class="form-control" value="<?php echo $direccion ?>" required>
						</div>
						<button type="submit" name="crear" class="btn btn-danger">Insertar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
