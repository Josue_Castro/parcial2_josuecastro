<?php

    $producto = new Producto();
    $productos = $producto -> consultarTodos();

    $tienda = new Tienda();
    $tienda -> consultarTodos();

    $cantidad = "";
    if(isset($_POST["cantidad"])){
        $cantidad = $_POST["cantidad"];
    }

    $productoSe = "";
    if(isset($_POST["Comprod"])){
        $productoSe = $_POST["Comprod"];
    }



if(isset($_POST["cant"])){

    $TiendaP = new Tienda_Producto("",$_GET["idTienda"],$productoSe,$cantidad);
    $TiendaP -> insertar();

}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Proveer Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["cant"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/tienda/añadirT.php"). "&idTienda=" . $_GET["idTienda"]?>" method="post">
						<div class="form-group">
							<label>Productos</label>
							<br>
							<label>
								<select name="Comprod">
								<?php foreach ($productos as $selectProd){ ?>

										<option value="<?php echo $selectProd -> getIdProducto() ?>"><?php echo $selectProd -> getNombre() ?></option>

								<?php }?>
								</select>
							</label>
						</div>
						<div class="form-group">
							<label>Cantidad</label>
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $cantidad ?>" required>
						</div>
						<button type="submit" name="cant" class="btn btn-danger">Ingresar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
