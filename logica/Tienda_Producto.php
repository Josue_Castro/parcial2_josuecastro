<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/Tienda_ProductoDAO.php';

class Tienda_Producto{

  private $idTienda_Poducto;
  private $idTienda;
  private $idProducto;
  private $cantidad;
  private $conexion;
  private $Tienda_ProductoDAO;


  public function getIdTienda_Producto(){
      return $this -> idTienda_Poducto;
  }

  public function getCantidad(){
      return $this -> cantidad;
  }

  public function Tienda_Producto($idTienda_Poducto = "", $idTienda = "", $idProducto = "", $cantidad = ""){
      $this -> idTienda_Poducto = $idTienda_Poducto;
      $this -> idTienda = $idTienda;
      $this -> idProducto = $idProducto;
      $this -> cantidad = $cantidad;
      $this -> conexion = new Conexion();
      $this -> Tienda_ProductoDAO = new Tienda_ProductoDAO($this -> idTienda_Poducto, $this -> idTienda, $this -> idProducto, $this -> cantidad);
  }

  public function consultar(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> Tienda_ProductoDAO -> consultar());
      $this -> conexion -> cerrar();
      $resultado = $this -> conexion -> extraer();
      $this -> idTienda = $resultado[0];
      $this -> idProducto = $resultado[1];
      $this -> cantidad = $resultado[2];
  }

  public function insertar(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> Tienda_ProductoDAO -> insertar());
      $this -> conexion -> cerrar();
  }

  public function consultarProductos_Tienda(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> Tienda_ProductoDAO -> consultarProductos_Tienda());
      $this -> conexion -> cerrar();
      $resultado = $this -> conexion -> extraer();
      $this -> idProducto = $resultado[1];
      $this -> cantidad = $resultado[2];
  }

  public function consultarTienda_Productos(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> Tienda_ProductoDAO -> consultarTienda_Productos());
      $this -> conexion -> cerrar();
  }



}






?>
