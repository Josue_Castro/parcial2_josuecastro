<?php
require "fpdf/fpdf.php";
require "logica/Producto.php";

$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Parcial", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte Tiendas", 0, 2, "C");


$idProducto = $_GET["idProducto"];
$pdf->Ln();
$pdf -> SetFont("Courier", "B", 10);
$pdf->Cell(50,12,"Nombre",1,0,'L',0);
$pdf->Cell(20,12,"Precio",1,0,'L',0);
$pdf->Cell(30,12,"Imagen",1,0,'L',0);
$pdf->Ln();
foreach ($productos as $filas) {
    $pdf -> SetFont("Courier", "B", 10);

    if($filas -> getImagen()!=""){
      $imagen = $filas -> getImagen();
    }

    $nombre = $filas -> getNombre();
    $precio = $filas -> getPrecio();

    $pdf->Cell(50,40,$nombre,1,0,'L',0);
    $pdf->Cell(20,40,$precio,1,0,'L',0);
    $pdf->Cell( 30,40, $pdf->Image($imagen, $pdf->GetX()+5, $pdf->GetY()+3, 20), 1, 0, 'C', false );
$pdf->Ln();
}


$pdf -> Output();

?>
