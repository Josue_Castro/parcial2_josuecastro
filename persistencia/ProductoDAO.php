<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $precio;

    public function ProductoDAO($idProducto = "", $nombre = "", $precio = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
    }

    public function consultar(){
        return "select nombre
                from producto
                where idProducto = '" . $this -> idProducto .  "'";
    }

    public function insertar(){
        return "insert into producto (nombre,precio)
                values ('" . $this -> nombre . "', '" . $this -> precio . "')";
    }

    public function consultarTodos(){
        return "select idProducto, nombre, precio
                from producto";
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, precio
                from producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from producto";
    }

    public function consultarFiltro($filtro){
        return "select idProducto, nombre, cantidad, precio
                from producto
                where nombre like '%" . $filtro . "%' or cantidad like '" . $filtro . "%' or precio like '" . $filtro . "%'";
    }

}

?>
